/*
 * config.h
 *
 *  Created on: 30-Jul-2021
 *      Author: Ajmi
 *      @about A single config file to configure the lorawan parameters without editing the main files.
 */

#ifndef WEATHER_STATION_CONFIG_H_
#define WEATHER_STATION_CONFIG_H_

#ifndef OVER_THE_AIR_ACTIVATION
#define OVER_THE_AIR_ACTIVATION                            0
#endif



/************************************Extra features that can be enabled here**********************************/
//Enable and Disable Modules

//#define ENABLE_SI1145_UVINDEX 			    /* uncomment to enable comment to disable */
//#define ENABLE_BME280					/* uncomment to enable comment to disable */
#define ENABLE_SHT20

/************************************Extra features that can be enabled here**********************************/


/************************************Device Key Configuration***************************************/

#define DEVICE_EUI 							{ IEEE_OUI, 0xc1, 0x3b, 0x44, 0xb5, 0x65 }

#define JOIN_EUI                            {0x01, 0x02, 0x0A, 0x0B, 0x0D, 0x00, 0x07, 0x09}

#define OTAA_APP_KEY						{0xD5, 0x8B, 0xC4, 0xDE, 0x99, 0xE1, 0x84, 0x38, 0xD4, 0xCC, 0xCF, 0x15, 0x20, 0x48, 0xA1, 0x9D}


#if !OVER_THE_AIR_ACTIVATION

#define DEVICE_ADDRESS 						( uint32_t )0xfc002700

#define NETWORK_SESSION_KEY					{0x8E, 0x06, 0x31, 0x67, 0xB9, 0x98, 0x98, 0xE5, 0x31, 0xEB, 0x8D, 0x74, 0x25, 0x60, 0xAF, 0x6A}

#define APP_SESSION_KEY						{0x2C, 0x06, 0x1F, 0x8A, 0x09, 0x2F, 0x9A, 0x2B, 0x0F, 0x97, 0xFB, 0xA4, 0x79, 0xBC, 0x33, 0x22}

#endif

/************************************Device Key Configuration***************************************/



/************************************Device Operation Configuration***************************************/

#define SEND_INTERVAL						30000              /*the application data transmission duty cycle*/

#define ADR_STATE 							LORAWAN_ADR_OFF	   /*LoRaWAN Adaptive Data Rate * @note Please note that when ADR is enabled the end-device should be static  check commissioning.h*/

#define DATA_RATE							DR_5

#define APP_PORT							2

#define CLASS_OPERATION						CLASS_A

#define UPLINK_MSG_STATE					LORAWAN_UNCONFIRMED_MSG

/************************************Device Operation Configuration***************************************/



#endif /* WEATHER_STATION_CONFIG_H_ */
