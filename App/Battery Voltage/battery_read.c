/*
 * battery_read.c
 *
 *  Created on: Jan 29, 2022
 *      Author: jaroos
 */

#include <string.h>
#include <stdlib.h>
#include "hw.h"

#include "battery_read.h"
#include "util_console.h"
#include "lora.h"
#include "i2c.h"

uint16_t readBatteryVoltage(void) {
	int analogValue = 0; /*   adc reading for battery is stored in the variable  */

	float average = 0;
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0; /*    battery voltage   */



	/* Read battery voltage reading */
	for (int i=0;i<10;i++){
		analogValue += HW_AdcReadChannel(ADC_CHANNEL_0);}

	average = analogValue/10;

	/*battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2 */
	batteryVoltage = (average * 4 * 3.3)/ 4096;
	PRINTF("Analog Value = %d\n",analogValue);

	/*multiplication factor of 100 to convert to int from float*/
	batteryLevel = (uint16_t) (batteryVoltage * 100);

	return batteryLevel;
}


void readBatteryLevel(battery_v *battery_data) {

	battery_data->batteryLevel = readBatteryVoltage();
}

